const express = require('express');
const app = express();
const dotenv = require('dotenv');

// Configuring the path
dotenv.config({ path: './config.env' });

//
app.use(express.json());

//
const chatRoute = require('./routes/chatRoutes');

//
app.use('/api/v1', chatRoute);

app.listen(process.env.PORT, () => {
  console.log('App running on port number 5000');
});
