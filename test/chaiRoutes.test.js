const { expect } = require('chai');
const sinon = require('sinon');
const { postquery } = require('../controller/chatController');
require('dotenv').config({ path: __dirname + '/../config/config.env' });

describe('Chat Routes', () => {
  afterEach(() => {});
  it('shoule send a response to given query', async () => {
    const req = {
      body: {
        query: 'What is html',
      },
    };
    const res = {
      statusCode: null,
      data: null,
      status: function (code) {
        this.statusCode = code;
        return this;
      },
      json: function (data) {
        this.data = data;
      },
    };
    await postquery(req, res, () => {});
    expect(res.statusCode).to.equal(200);
    expect(res.data.success).to.be.true;
  });

  it('should throw an error if empty object is sent in a query', async () => {
    const req = {
      body: {
        query: {},
      },
    };
    const res = {
      statusCode: null,
      data: null,
      status: function (code) {
        this.statusCode = code;
        return this;
      },
      json: function (data) {
        this.data = data;
      },
    };
    await postquery(req, res, () => {});
    expect(res.data.success).to.be.false;
  });
});
