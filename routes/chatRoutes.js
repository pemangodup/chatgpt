const express = require('express');
const router = express.Router();
const { postquery } = require('../controller/chatController');

router.route('/chat').post(postquery);
module.exports = router;
