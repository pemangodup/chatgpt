## ChatGPT API clone. It is a simple api created using basic tools.

### Tools and package used are:

1. NodeJS
2. Express Framework
3. Mocha
4. Chai
5. Sinon
6. OpenAI API

### Testing Tools

- Postman

### Endpoint to fetch api

_API endpoint_: `http://localhost:5000/api/v1/chat`

### Getting OpenAI API

: First, we will need to sign up for the OpenAI API and obtain an API key. We can do this by visiting the OpenAI website and following the instructions for creating an account. Once we have our API key we can store it in .env file as a variable.

**Controller Codes**

```
const { Configuration, OpenAIApi } = require('openai');

const chatController = {
  postquery: async (req, res, next) => {
    const { query } = req.body;
    const configuration = new Configuration({
      apiKey: process.env.OPENAI_API,
    });
    const openai = new OpenAIApi(configuration);
    try {
      const completion = await openai.createCompletion({
        model: 'text-davinci-003',
        prompt: query,
        max_tokens: 4000,
      });
      res.status(200).json({
        success: true,
        data: completion.data.choices[0].text,
      });
    } catch (error) {
      if (error.response) {
        res.status(error.response.status).json({
          success: false,
          data: error.response.data,
        });
      } else {
        res.status(404).json({
          success: false,
          data: error.message,
        });
      }
    }
  },
};

module.exports = chatController;

```

**New major projects coming soon. Subscribe to my youtube channel for detail coding.**
[Channel Link: ](https://www.youtube.com/channel/UCRqMFivSFIa7GBWVniYwkQA)
